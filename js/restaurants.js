//overwrites contains jQuery para que sea case insensitive
jQuery.expr[":"].Contains = jQuery.expr.createPseudo(function(arg) {
    return function( elem ) {
        return jQuery(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

var map;
//Diccionario que contendrá un objeto de (latitud,longitud) de cada hotel
//Así nos ahorramos la petición AJAX cada vez
var dicPosition = {};


//Función para mostrar el mapa para uno
function myMap(latitud,longitud,i) {
    var myCenter = new google.maps.LatLng(latitud,longitud);
    var mapCanvas = document.getElementById("googleMap"+i);
    var mapOptions = {center: myCenter, zoom: 10};
    map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position:myCenter,
        map: map,
        title: 'PRUEBA'
    });
}
function filtrado(name, city, priceMin,priceMax,rating){
  var resultsContainer =$("#resultsContainer div[id^=hotel]");
  //var originalResults = $("#resultsContainer div[id^=hotel]");

  //Filtramos los resultados name
  var resultsToShow = resultsContainer.find('h4:Contains('+ name +')').closest('div');
  var resultsToHide = resultsContainer.find('h4:not(:Contains('+ name +'))').closest('div');
  resultsToHide.hide();
  resultsToShow.show();

  //Filtrar resultados city
  var resultsContainer =  $("#resultsContainer div[id^=hotel]:visible");
  var resultsToShow = resultsContainer.find('p:Contains('+ city+')').closest('div');
  var resultsToHide = resultsContainer.find('p:not(:Contains('+ city +'))').closest('div');
  resultsToHide.hide();
  resultsToShow.show();

  //Filtramos los prices
  var resultsContainer =  $("#resultsContainer div[id^=hotel]:visible");
  var resultsToShow = resultsContainer.filter(function(idx){
      var curr = parseInt($(this).find(".price").attr('data-price'));
      return priceMin <= curr && curr <= priceMax;
  });
  var resultsToHide = resultsContainer.filter(function(idx){
      var curr = parseInt($(this).find(".price").attr('data-price'));
      return !(priceMin <= curr && curr <= priceMax);
  });
  resultsToHide.hide();
  resultsToShow.show();

  //Filtramos el rating
  if(rating != 0){
  var resultsContainer =  $("#resultsContainer div[id^=hotel]:visible");
  var resultsToShow = resultsContainer.filter(function(idx){
      var curr = parseInt($(this).find("div.rating").attr('data-rating'));
      return rating == curr;
  });
  var resultsToHide = resultsContainer.filter(function(idx){
      var curr = parseInt($(this).find("div.rating").attr('data-rating'));
      return rating != curr;
  });
  resultsToHide.hide();
  resultsToShow.show();
}
  return false

}
//Función para mostrar el mapa de todos
function myMapAll(allPositions,namesHotels){
    var maxWidthInfoWindow = 200;
    var idx = 0
    $.each(allPositions,function (key,value){
        var nameHotel = namesHotels[key]; //Comparten key allPositions y namesHotels
        var htmlHotel = '<div class="row" >' +
        '<div id="" class="col-md-12">' +
        '<h4>'+ nameHotel +'</h4>' +
        '</div>'+
        '</div>';
        var positionHotel = new google.maps.LatLng(value.latitud,value.longitud);
        //Inicializar el mapa si es la primera vez
        if (idx == 0){
            var mapCanvas = document.getElementById("googleMapAll");
            var mapOptions = {center: positionHotel, zoom: 10};
            map = new google.maps.Map(mapCanvas, mapOptions);
        }

        var marker = new google.maps.Marker({
            position:positionHotel,
            map: map,
            title: 'Hotel'+idx
        });
        var infowindow = new google.maps.InfoWindow({
            content: htmlHotel,
            maxWidth: maxWidthInfoWindow
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });
        marker.setMap(map);
        idx = idx + 1;
    });
    return false;
}
//Validar el comentario de un hotel antes de hacer el submit
function validarComentario(elem){
    var validado = true;
    var inputName = $(elem).find("#inputName");
    var inputComentario = $(elem).find("#inputComentario");
    var comenRating = $(elem).find("#comenRating");
    var comenRatingValue = $(elem).find("#comenRatingValue");
    var inputTituloComentario = $(elem).find("#inputTituloComentario");
    //Validation states en CSS bootstrap
    if (!inputName.val()){
        validado = false;
        inputName.parent().addClass("has-error");
    } else {
        inputName.parent().removeClass("has-error");
    }
    if (!inputComentario.val()) {
        validado = false;
        inputComentario.parent().addClass("has-error");
    } else {
        inputComentario.parent().removeClass("has-error");
    }
    if (!inputTituloComentario.val()) {
        validado = false;
        inputTituloComentario.parent().addClass("has-error");
    } else {
        inputTituloComentario.parent().removeClass("has-error");
    }
    if (comenRating.starRating('getRating') == 0) {
        validado = false;
        alert("Hay que valorar el restaurante");
    } else {
        comenRatingValue.val(comenRating.starRating('getRating'));
    }
    return validado;
}


function getLocationAJAX(idRestaurant){
    $.ajax({
        dataType: "json",
        async: false,
        url: "../php/handlerRestaurants.php",
        data: {action: "getLocation",idRestaurant: idRestaurant},
        success: function(result){
            //Guardamos en el dic el resultado obtenido
            dicPosition["hotelInfo"+idRestaurant] = result;
            myMap(result.latitud,result.longitud,idRestaurant);
        },
        error: function(result){
        }
    });

    return false;
}
//Abrir infoHotel en vista normal
function infoClickEvent(){
    $("#resultsContainer .glyphicon-info-sign").click(function(){
    var actualHotel = $(this).closest("div[id^=hotel]").prop("id");
    var numId = actualHotel.substring(actualHotel.length -1,actualHotel.length);
    var hotelInfo = $("div[id=hotelInfo"+ numId +"]");
    //Solo lo abrimos si no hay ninguno abierto
    if (!hotelInfo.dialog("isOpen")) {
        hotelInfo.find("#myCarousel" + numId).toggleClass("hidden");
        hotelInfo.dialog("open");
    }
});
}
$(document).ready(function(){
    //variable que contiene los resultados de la búsqueda
    var originalResults = $("#resultsContainer div[id^=hotel]");
    //Esta variable es la que va cambiando el contenido en función del filtrado y la ordenación
    var resultsContainer = $("#resultsContainer");

    infoClickEvent();


    // Add smooth scrolling to all links in navbar + footer link
    $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 900, function(){

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
    //Evento del filtro de nombre
    $("#filterName").keyup(function(evt){
      var name = $("#filterName").val();
      var city = $("#filterCity").val();
      var priceMin = $( "#filterPrice" ).slider( "option", "values" )[0];
      var priceMax = $( "#filterPrice" ).slider( "option", "values" )[1];
      var rating = $('#filterRating').starRating('getRating');

      filtrado(name,city,priceMin,priceMax,rating);

    });
    //Evento del filtro de nombre
    $("#filterCity").keyup(function(evt){
      var name = $("#filterName").val();
      var city = $("#filterCity").val();
      var priceMin = $( "#filterPrice" ).slider( "option", "values" )[0];
      var priceMax = $( "#filterPrice" ).slider( "option", "values" )[1];
      var rating = $('#filterRating').starRating('getRating');

      filtrado(name,city,priceMin,priceMax,rating);

    });
    var maxPrice = 0;
    var minPrice = 0;
    //Buscar minimo y máximo del precio de hotel
    originalResults.each(function(idx,element){
        var curr = parseInt($(element).find(".price").attr('data-price'));
        if (idx == 0){
            maxPrice = curr;
            minPrice = curr;
        }
        if (curr < minPrice){
            minPrice = curr;
        } else if (curr > maxPrice) {
            maxPrice = curr;
        }
    });
    $("#filterPrice").slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [ minPrice, maxPrice ],
        slide: function(evt, ui){
            var prices = $( "#filterPrice" ).slider( "option", "values" );
            $('#filterP').val(prices[0] + "EUR - " + prices[1] + "EUR");
        },
        change: function(evt, ui){
          var name = $("#filterName").val();
          var city = $("#filterCity").val();
          var priceMin = $( "#filterPrice" ).slider( "option", "values" )[0];
          var priceMax = $( "#filterPrice" ).slider( "option", "values" )[1];
          var rating = $('#filterRating').starRating('getRating');

          filtrado(name,city,priceMin,priceMax,rating);
        }
    });
    $("#filterRating").starRating({
        starSize: 40,
        disableAfterRate: false,
        useFullStars: true,
        callback: function(currentRating, element){
           var name = $("#filterName").val();
                  var city = $("#filterCity").val();
                  var priceMin = $( "#filterPrice" ).slider( "option", "values" )[0];
                  var priceMax = $( "#filterPrice" ).slider( "option", "values" )[1];
                  var rating = $('#filterRating').starRating('getRating');

                  filtrado(name,city,priceMin,priceMax,rating);
        }
    });
    //Submit de cada form
    $("div[id=comentarioForm]").find("form").each(function (idx,elem){
      $(elem).submit(function(e){
        e.preventDefault(); // avoid to execute the actual submit of the form.
        if (validarComentario(elem) == true){
          var url = '../php/handlerRestaurants.php';
          $.ajax({
           type: "GET",
           url: url,
           data: $(elem).serialize(), // serializes the form's elements.
           success: function(data){
               alert("El comentario de ha guardado correctamente"); // show response from the php script.
           },
           error: function(){
             alert("Ha ocurrido un error"); // show response from the php script.
           }
         });
       }
      });

    });
    $("div[id=comentarioForm]").find("div[id=comenRating]").each(function(idx,elem){
      //Rating del comentario del hotel del form
      $(elem).starRating({
          starSize: 20,
          disableAfterRate: false,
          useFullStars: true,
      });
    });
    //Rating de cada comentario por cada hotelInfo buscamos los comentarios que hay
    //Y creamos el div del rating
     $("div[id^=hotelInfo]").find("div[id^=rating]").each(function(idx,elem){
       var rating = parseInt($(elem).attr('data-rating'));
       $(elem).starRating({
           starSize: 20,
           readOnly: true,
           useFullStars: true,
           initialRating: rating, //$('your-selector').starRating('setRating', 3)
       });
    });
    // //Se tendrá que hacer en función de la valoración de cada comentario
    // $("#rating1").starRating({
    //     starSize: 20,
    //     readOnly: true,
    //     useFullStars: true,
    //     initialRating: 4, //$('your-selector').starRating('setRating', 3)
    // });
    // $("#rating2").starRating({
    //     starSize: 20,
    //     readOnly: true,
    //     useFullStars: true,
    //     initialRating: 1, //$('your-selector').starRating('setRating', 3)
    // });
    $(".rating").starRating({
        starSize: 20,
        readOnly: true,
        useFullStars: true,
        initialRating: 4, //$('your-selector').starRating('setRating', 3)
    });
    //Setear el rating
    originalResults.each(function(idx,element){
        var stars = parseInt($(element).find("div.rating").attr('data-rating'));
        $(element).find("div.rating").starRating("setRating",stars);
    });

    $("#hideFilter").click(function() {
        $("#filter").toggle("slide", function(){
            //Ampliamos los resultados
            if ($("#results").hasClass("col-md-8")){
                $("#results").removeClass("col-md-8").addClass("col-md-11");
            } else {
                $("#results").removeClass("col-md-11").addClass("col-md-8");
            }

        });
    });
    //Cambiar entre la vista de mapa o la normal
    $("#buttonMap").click(function(){
        var buttonMap = $(this);
        $("#viewNoMap").toggle(function(){
            if($(this).is(":visible")){
                buttonMap.val("Vista en mapa");
            }
        });
        $("#viewMap").toggle(function(){
            if($(this).is(":visible")){
                //Creamos el mapa de todos, por cada resultado creamos un marcador con su infowindow
                //E inicializamos el mapa
                var namesHotels = {};
                originalResults.each(function(idx,element){
                    var i = idx +1;
                    var position = dicPosition["hotelInfo"+i];
                    namesHotels["hotelInfo"+i] = $(element).find("h4").text();
                    //Si existe lo metemos en el array que le pasaremos a myMapAll
                    //Sino hacemos la petición AJAX
                    if (position == undefined) {
                        getLocationAJAX(i);
                    }
                });
                myMapAll(dicPosition,namesHotels);
                buttonMap.val("Vista normal");
            }
        });
    });
    $(".hotelsInfos div[id^=hotelInfo]").each(function(idx,element){
        var i = idx +1;
        //crear un dialog por cada resultado
        //Init dialog que muestra info del hotel
        $("#hotelInfo"+i).dialog({
            autoOpen:false,
            height: 800,
            width: 800,
            resizable: false,
            draggable: false,
            modal: true,
            open: function (evt, ui) {
                var position = dicPosition["hotelInfo"+i];
                //Refresh del mapa para que lo pinte
                //google.maps.event.trigger(map, 'resize');
                //Hay que pasarle la latitud y longitud del hotel que toca
                if (position == undefined){
                    getLocationAJAX(i);
                } else {
                    google.maps.event.trigger(map, 'resize');
                    map.setCenter(new google.maps.LatLng(position.latitud,position.longitud));
                    map.setZoom( map.getZoom() );
                }
            },
            close: function(evt, ui) {
                $("#myCarousel"+i).toggleClass("hidden");
            }
        });

    });

    //Ordenación ascendente por nombre
    $("#orderName .glyphicon-chevron-up").parent().click(function(){
        var htmlOrderedUp = originalResults.sort(function(a,b){
            return $(a).find("h4").text().toLowerCase() > $(b).find("h4").text().toLowerCase();
        });
        resultsContainer.html(htmlOrderedUp);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación descendente por nombre
    $("#orderName .glyphicon-chevron-down").parent().click(function(){
        var htmlOrderedDown = originalResults.sort(function(a,b){
            return $(a).find("h4").text().toLowerCase() < $(b).find("h4").text().toLowerCase();
        });
        resultsContainer.html(htmlOrderedDown);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación ascendente por ciudad
    $("#orderCity .glyphicon-chevron-up").parent().click(function(){
        var htmlOrderedUp = originalResults.sort(function(a,b){
            return $(a).find("p.city").text().toLowerCase() > $(b).find("p.city").text().toLowerCase();
        });
        resultsContainer.html(htmlOrderedUp);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación descendente por ciudad
    $("#orderCity .glyphicon-chevron-down").parent().click(function(){
        var htmlOrderedDown = originalResults.sort(function(a,b){
            return $(a).find("p.city").text().toLowerCase() < $(b).find("p.city").text().toLowerCase();
        });
        resultsContainer.html(htmlOrderedDown);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación ascendente por precio
    $("#orderPrice .glyphicon-chevron-up").parent().click(function(){
        var htmlOrderedUp = originalResults.sort(function(a,b){
            return $(a).find("p.price").attr('data-price') > $(b).find("p.price").attr('data-price');
        });
        resultsContainer.html(htmlOrderedUp);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación descendente por precio
    $("#orderPrice .glyphicon-chevron-down").parent().click(function(){
        var htmlOrderedDown = originalResults.sort(function(a,b){
            return $(a).find("p.price").attr('data-price') < $(b).find("p.price").attr('data-price');
        });
        resultsContainer.html(htmlOrderedDown);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación ascendente por rating
    $("#orderRating .glyphicon-chevron-up").parent().click(function(){
        var htmlOrderedUp = originalResults.sort(function(a,b){
            return $(a).find("div").attr('data-rating') > $(b).find("div").attr('data-rating');
        });
        resultsContainer.html(htmlOrderedUp);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });
    //Ordenación descendente por rating
    $("#orderRating .glyphicon-chevron-down").parent().click(function(){
        var htmlOrderedDown = originalResults.sort(function(a,b){
            return $(a).find("div").attr('data-rating')  < $(b).find("div").attr('data-rating') ;
        });
        resultsContainer.html(htmlOrderedDown);
        //Volver a añadir el evento de click al info
        infoClickEvent();
    });

    $(window).scroll(function() {
        $(".slideanim").each(function(){
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });

    checkForIndexSearch();
})

function checkForIndexSearch(){
    if(document.URL.includes("search=")){
        var searchFor = document.URL.split("search=")[1];
        document.getElementById("filterName").value = searchFor;
        $("#filterName").keyup();
    }
}

function changeLanguage(newLang){
    document.location="restaurants.php?lang=" + newLang;
}

function callIndexPHP(){
    document.location = document.URL.replace('restaurants','index').split("&search=")[0];
}

function callHotelsPHP(){
    document.location = document.URL.replace('restaurants','hotels').split("&search=")[0];
}

function callPubsPHP(){
    document.location = document.URL.replace('restaurants','pubs').split("&search=")[0];
}

function callRestaurantsPHP(){
    document.location = document.URL.replace('restaurants','restaurants').split("&search=")[0];
}

function callSpasPHP(){
    document.location = document.URL.replace('restaurants','spas').split("&search=")[0];
}
