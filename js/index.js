$(document).ready(function(){
    $(window).scroll(function() {
        $(".slideanim").each(function(){
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });
})

function changeLanguage(newLang){
    document.location="index.php?lang=" + newLang;
}

function applySearch(){
    var service = document.getElementById("service").selectedIndex;
    var search = document.getElementById("search").value;
    
    switch(service){
        case 0:
            service = "hotels";
            break;
        case 1:
            service = "pubs";
            break;
        case 2:
            service = "restaurants";
            break;
        case 3:
            service = "spas";
            break;
    }
    
    document.location = document.URL.replace('index', service) + "&search=" + search;
}

function callIndexPHP(){
    document.location = document.URL.replace('index','index');
}

function callHotelsPHP(){
    document.location = document.URL.replace('index','hotels');
}

function callPubsPHP(){
    document.location = document.URL.replace('index','pubs');
}

function callRestaurantsPHP(){
    document.location = document.URL.replace('index','restaurants');
}

function callSpasPHP(){
    document.location = document.URL.replace('index','spas');
}