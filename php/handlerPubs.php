<?php
    $action = $_GET["action"];
    $idPub = $_GET["idPub"];

    switch ($action) {
        case 'getLocation':
            $xml=simplexml_load_file("../data/pubs.xml") or die("Error: Cannot create object");

            $latitud = $xml->xpath('pub[@id='.$idPub.']/position/latitude')[0];
            $longitud = $xml->xpath('pub[@id='.$idPub.']/position/longitude')[0];

            $return = array(
            latitud => (float)$latitud,
            longitud => (float)$longitud,
            );
            echo json_encode($return);
            break;
        case 'saveComment':
            $user = $_GET["name"];
            $title = $_GET["tituloComentario"];
            $description = $_GET["comentario"];
            $rate = $_GET["comenRatingValue"];

            $xml=new SimpleXMLElement("../data/comments.xml", null, true);
            $comment = $xml->pubs[0]->addChild('comment');
            $comment->addAttribute('idTo',$idPub);
            $comment->addAttribute('rate',$rate);
            $comment->addAttribute('date',date('Y-m-d'));
            $comment->addChild('user',$user);
            $comment->addChild('title', $title);
            $comment->addChild('description',$description);
            $xml->asXML('../data/comments.xml');
            //echo true
            // header('Content-type: text/xml');
            // echo $xml->asXML();
                # code...
            break;
        default:
            # code...
            break;
    }

?>
