<?php
    $language = $_GET["lang"];
    $xsl_filename = "../xslt/hotels.xsl";

	$xml_filename = "../data/translations.xml";
    $xml_filename2 = "../data/hotels.xml";
    $xml_filename3 = "../data/comments.xml";



	$doc = new DOMDocument();
	$xsl = new XSLTProcessor();

	$doc->load($xsl_filename);

	$xsl->importStyleSheet($doc);
    $xsl->setParameter('', 'language', $language);

	$doc->load($xml_filename);


    echo $xsl->transformToXML($doc);
?>
