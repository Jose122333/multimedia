<?php
    $action = $_GET["action"];
    $idSpa = $_GET["idSpa"];

    switch ($action) {
        case 'getLocation':
            $xml=simplexml_load_file("../data/spas.xml") or die("Error: Cannot create object");

            $latitud = $xml->xpath('spa[@id='.$idSpa.']/position/latitude')[0];
            $longitud = $xml->xpath('spa[@id='.$idSpa.']/position/longitude')[0];

            $return = array(
            latitud => (float)$latitud,
            longitud => (float)$longitud,
            );
            echo json_encode($return);
            break;
        case 'saveComment':
            $user = $_GET["name"];
            $title = $_GET["tituloComentario"];
            $description = $_GET["comentario"];
            $rate = $_GET["comenRatingValue"];

            $xml=new SimpleXMLElement("../data/comments.xml", null, true);
            $comment = $xml->spas[0]->addChild('comment');
            $comment->addAttribute('idTo',$idSpa);
            $comment->addAttribute('rate',$rate);
            $comment->addAttribute('date',date('Y-m-d'));
            $comment->addChild('user',$user);
            $comment->addChild('title', $title);
            $comment->addChild('description',$description);
            $xml->asXML('../data/comments.xml');
            //echo true
            // header('Content-type: text/xml');
            // echo $xml->asXML();
                # code...
            break;
        default:
            # code...
            break;
    }

?>
