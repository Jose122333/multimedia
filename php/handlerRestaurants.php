<?php
    $action = $_GET["action"];
    $idRestaurant = $_GET["idRestaurant"];

    switch ($action) {
        case 'getLocation':
            $xml=simplexml_load_file("../data/restaurants.xml") or die("Error: Cannot create object");

            $latitud = $xml->xpath('restaurant[@id='.$idRestaurant.']/position/latitude')[0];
            $longitud = $xml->xpath('restaurant[@id='.$idRestaurant.']/position/longitude')[0];

            $return = array(
            latitud => (float)$latitud,
            longitud => (float)$longitud,
            );
            echo json_encode($return);
            break;
        case 'saveComment':
            $user = $_GET["name"];
            $title = $_GET["tituloComentario"];
            $description = $_GET["comentario"];
            $rate = $_GET["comenRatingValue"];

            $xml=new SimpleXMLElement("../data/comments.xml", null, true);
            $comment = $xml->restaurants[0]->addChild('comment');
            $comment->addAttribute('idTo',$idRestaurant);
            $comment->addAttribute('rate',$rate);
            $comment->addAttribute('date',date('Y-m-d'));
            $comment->addChild('user',$user);
            $comment->addChild('title', $title);
            $comment->addChild('description',$description);
            $xml->asXML('../data/comments.xml');
            //echo true
            // header('Content-type: text/xml');
            // echo $xml->asXML();
                # code...
            break;
        default:
            # code...
            break;
    }

?>
