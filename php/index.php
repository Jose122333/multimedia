<?php
    $language = $_GET["lang"];
    $xsl_filename = "../xslt/index.xsl";
	$xml_filename = "../data/translations.xml";
	
	$doc = new DOMDocument();
	$xsl = new XSLTProcessor();
	
	$doc->load($xsl_filename);
	
	$xsl->importStyleSheet($doc);
    $xsl->setParameter('', 'language', $language);

	$doc->load($xml_filename);

    echo $xsl->transformToXML($doc);
?>