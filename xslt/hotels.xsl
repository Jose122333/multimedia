<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl" version="1.0">
    <xsl:variable name="xml_comments" select="document('../data/comments.xml')/coments/hotels" />
    <xsl:variable name="xml_hotels" select="document('../data/hotels.xml')/hotels/hotel" />
    <xsl:variable name="xml_translations" select="document('../data/translations.xml')" />


    <xsl:template match='/'>
        <html lang='$language'>
            <head>
                <title>Q for quality</title>
                <meta charset="utf-8" />
                <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="../css/styles.css"/>
                <link rel="stylesheet" href="../css/font-awesome-4.7.0/css/font-awesome.min.css"/>
                <link rel="stylesheet" href="../jquery-ui/jquery-ui.css"/>
                <link rel="stylesheet" type="text/css" href="../star-rating/src/css/star-rating-svg.css"/>
                <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
                <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css"/>
                <script src="../jquery-ui/external/jquery/jquery.js"></script>
                <script src="../bootstrap/js/bootstrap.min.js"></script>
                <script src="../jquery-ui/jquery-ui.js"></script>
                <script src="../star-rating/src/jquery.star-rating-svg.js"></script>
                <script src="../js/hotels.js"></script>
            </head>
            <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"/>
                                <span class="icon-bar"/>
                                <span class="icon-bar"/>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a onclick='changeLanguage("de")'><img src="../images/svg/de.svg" height="30"/></a></li>
                                <li><a onclick='changeLanguage("en")'><img src="../images/svg/en.svg" height="30"/></a></li>
                                <li><a onclick='changeLanguage("es")'><img src="../images/svg/es.svg" height="30"/></a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a onclick='callIndexPHP();'><i class="fa fa-home fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="1"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callHotelsPHP();'><i class="fa fa-h-square fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="2"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callPubsPHP();'><i class="fa fa-cutlery fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="3"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callRestaurantsPHP();'><i class="fa fa-glass fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="4"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callSpasPHP();'><i class="fa fa-shower fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="5"]/*[name()=$language]'/></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div id="hotels" class="container-fluid">
                    <div id="viewNoMap" class="row">
                        <div class="col-md-3">
                            <!-- <button id="hideFilter" class="btn btn-default vertical-text"><xsl:value-of select='//text[@id="10"]/*[name()=$language]'/></button> -->
                            <div id="filter" >
                                <h4><xsl:value-of select='//text[@id="11"]/*[name()=$language]'/></h4>
                                <label for="filterName"><xsl:value-of select='//text[@id="12"]/*[name()=$language]'/>:</label>
                                <div class="form-group">
                                    <input id="filterName" type="text" class="form-control" placeholder="Escriba al menos 3 carácteres..."/>
                                </div>
                                <label for="filterCity"><xsl:value-of select='//text[@id="13"]/*[name()=$language]'/>:</label>
                                <div class="form-group">
                                    <input id="filterCity" type="text" class="form-control" placeholder="Escriba al menos 3 carácteres..."/>
                                </div>
                                <div class="form-group">
                                    <label for="filterP"><xsl:value-of select='//text[@id="14"]/*[name()=$language]'/>:</label>
                                    <input type="text" id="filterP"/>
                                    <div id="filterPrice"></div>
                                </div>
                                <div class="form-group">
                                    <label for="filterRating"><xsl:value-of select='//text[@id="15"]/*[name()=$language]'/>:</label>
                                    <div id="filterRating"></div>
                                </div>
                            </div>
                        </div>
                        <div id="results" class="col-md-8  text-center">
                            <h2><xsl:value-of select='//text[@id="16"]/*[name()=$language]'/></h2>
                            <div id="order">
                                <div class="row">
                                    <div id="orderName" class="col-md-2 col-md-offset-1">
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                                        </button>
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                        </button>
                                        <p><xsl:value-of select='//text[@id="12"]/*[name()=$language]'/></p>
                                    </div>
                                    <div id="orderCity" class="col-md-2 col-md-offset-1">
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                                        </button>
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                        </button>
                                        <p><xsl:value-of select='//text[@id="13"]/*[name()=$language]'/></p>
                                    </div>
                                    <div id="orderPrice" class="col-md-2 col-md-offset-1">
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                                        </button>
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                        </button>
                                        <p><xsl:value-of select='//text[@id="14"]/*[name()=$language]'/></p>
                                    </div>
                                    <div id="orderRating" class="col-md-2 col-md-offset-1">
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
                                        </button>
                                        <button class="btn btn-default btn-circle">
                                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                                        </button>
                                        <p><xsl:value-of select='//text[@id="15"]/*[name()=$language]'/></p>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div id="resultsContainer">
                                <xsl:for-each select="$xml_hotels">
                                    <div id="hotel{@id}" class="col-md-4">
                                        <xsl:variable name="photo" select="photos/photo[1]/@src"/>
                                        <img class="img-responsive img-rounded" height="150" src="../{$photo}"/>
                                        <h4>
                                          <xsl:value-of select="name"/>
                                          <span title="Ver más info" class="glyphicon glyphicon-info-sign">
                                          </span>
                                        </h4>
                                        <xsl:variable name="stars" select="@stars" />
                                        <div class="rating" data-rating="{$stars}"></div>
                                        <xsl:variable name="price" select="@avgPrice" />
                                        <p class="price" data-price="{$price}">
                                            <xsl:value-of select="$xml_translations//text[@id='28']/*[name()=$language]"/>:
                                            <xsl:value-of select="@avgPrice"/>€
                                        </p>
                                        <p class="city"><xsl:value-of select="position/city"/></p>
                                    </div>
                                </xsl:for-each>
                            </div>
                        </div>

                    </div>
                    <!-- Se pone en style porque en class el toggle del js no funciona correctamente -->
                    <div id="viewMap" class="row" style="display:none;">
                        <div id="googleMapAll" class="map"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 col-md-offset-11">
                            <input id="buttonMap" class="btn btn-default" type="button" value="Ver en mapa"/>
                        </div>
                    </div>
                </div>
                <div class="hotelsInfos">
                    <xsl:for-each select="$xml_hotels">
                        <xsl:variable name="currId" select="@id" />
                        <xsl:variable name="shortDescrID" select="//hotel[@id=$currId]/shortDescription/@textID"/>
                        <xsl:variable name="longDescrID" select="//hotel[@id=$currId]/description/@textID"/>
                        <xsl:variable name="url" select="//hotel[@id=$currId]/url"/>

                        <!-- Div usado para mostrar la info del hotel seleccionado, hay una por cada hotel -->
                        <div id="hotelInfo{$currId}" title="Información hotel">
                            <div id="myCarousel{$currId}" class="carousel slide hidden" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel{$currId}" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel{$currId}" data-slide-to="1"></li>
                                    <li data-target="#myCarousel{$currId}" data-slide-to="2"></li>
                                    <li data-target="#myCarousel{$currId}" data-slide-to="3"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <xsl:for-each select="photos/photo">
                                        <xsl:choose>
                                            <xsl:when test="position()=1">
                                                <div class="item active">
                                                    <img src="../{@src}" alt="Chania" width="460" height="345"/>
                                                    <div class="carousel-caption">
                                                        <h3><xsl:value-of select='//hotel[@id=$currId]/name'/></h3>
                                                        <p><xsl:value-of select='$xml_translations//text[@id=$shortDescrID]/*[name()=$language]'/></p>
                                                    </div>
                                                </div>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <div class="item">
                                                    <img src="../{@src}" alt="Chania" width="460" height="345"/>
                                                    <div class="carousel-caption">
                                                        <h3><xsl:value-of select='//hotel[@id=$currId]/name'/></h3>
                                                        <p><xsl:value-of select='$xml_translations//text[@id=$shortDescrID]/*[name()=$language]'/></p>
                                                    </div>
                                                </div>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:for-each>
                                </div>
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel{$currId}" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel{$currId}" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                            <h1><xsl:value-of select="$xml_translations//text[@id='17']/*[name()=$language]"/></h1>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <b style="font-size:18px;">Web</b>
                                    <a href="{$url}"><p><xsl:value-of select='//hotel[@id=$currId]/url'/></p></a>
                                </li>
                                <li class="list-group-item">
                                    <b style="font-size:18px;"><xsl:value-of select='$xml_translations//text[@id="18"]/*[name()=$language]'/></b>
                                    <p><xsl:value-of select='contact/telf'/></p>
                                </li>
                                <li class="list-group-item">
                                    <b style="font-size:18px;"><xsl:value-of select='$xml_translations//text[@id="19"]/*[name()=$language]'/></b>
                                    <p><xsl:value-of select='contact/direction'/></p>
                                </li>
                                <li class="list-group-item">
                                    <b style="font-size:18px;">Email</b>
                                    <a href="mailto:{contact/mail}?Subject=Hello%20again" target="_top"><p><xsl:value-of select='contact/mail'/></p></a>
                                </li>
                            </ul>
                            <h1><xsl:value-of select='$xml_translations//text[@id="20"]/*[name()=$language]'/></h1>
                            <p><xsl:value-of select='$xml_translations//text[@id=$longDescrID]/*[name()=$language]'/></p>
                            <h2><xsl:value-of select='$xml_translations//text[@id="21"]/*[name()=$language]'/></h2>
                            <div class="list-group">
                                <!-- Aquí hay que pillar los servicios de cada hotel de los nodos-->
                                <xsl:for-each select="services">
                                    <xsl:if test="wifi">
                                        <span class="list-group-item fa fa-wifi fa-lg" title="Wifi" aria-hidden="true"></span>
                                    </xsl:if>
                                    <xsl:if test="spa">
                                        <span class="list-group-item fa fa-bath fa-lg" title="Bañera" aria-hidden="true"></span>
                                    </xsl:if>
                                    <xsl:if test="parking">
                                        <span class="list-group-item fa fa-car fa-lg" title="Parking" aria-hidden="true"></span>
                                    </xsl:if>
                                    <xsl:if test="kids">
                                        <span class="list-group-item fa fa-users fa-lg" title="Kids allowed" aria-hidden="true"></span>
                                    </xsl:if>
                                </xsl:for-each>
                            </div>
                            <div id="comentarioSection">
                                <div id="comentarios">
                                    <h2><xsl:value-of select='$xml_translations/translations/text[@id="22"]/*[name()=$language]'/></h2>
                                    <xsl:for-each select='$xml_comments/comment[@idTo=$currId]'>
                                        <div class="row">
                                            <div class="col-md-1 col-md-offset-2">
                                                <div class="name">
                                                    <p><xsl:value-of select='user'/></p>
                                                </div>
                                            </div>

                                            <div id="rating{$currId}" data-rating="{@rate}">
                                                <!-- solo para guardar el valor del rating del XML -->
                                                <input value="{@rate}" class="hidden"/>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <div id="inputTituloComentario1" class="tituloComentario">
                                                    <p><xsl:value-of select='title'/></p>
                                                </div>
                                                <div id="inputComentario1" class="comentario">
                                                    <p><xsl:value-of select='description'/></p>
                                                </div>
                                            </div>
                                        </div>
                                    </xsl:for-each>
                                </div>
                                <div id="comentarioForm">
                                    <form action="handlerHotels.php" method="get" onsubmit="">
                                        <input type="hidden" name="action" value="saveComment"/>
                                        <input type="hidden" name="idHotel" value="{$currId}"/>
                                        <div class="row">
                                            <div class="col-md-4 col-md-offset-2 ">
                                                <div class="form-group">
                                                    <label for="inputName"><xsl:value-of select='$xml_translations//text[@id="24"]/*[name()=$language]'/></label>
                                                    <input type="Text" name="name" class="form-control" id="inputName" placeholder="Nombre"/>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <label for="comenRating"><xsl:value-of select='$xml_translations//text[@id="25"]/*[name()=$language]'/></label>
                                                <div id="comenRating"></div>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <div class="form-group">
                                                    <label for="inputTituloComentario"><xsl:value-of select='$xml_translations//text[@id="26"]/*[name()=$language]'/></label>
                                                    <input type="Text" name="tituloComentario" class="form-control" id="inputTituloComentario" placeholder="Resumen de tu experiencia"/>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <div class="form-group">
                                                    <label for="inputComentario"><xsl:value-of select='$xml_translations//text[@id="27"]/*[name()=$language]'/></label>
                                                    <textarea type="Text" name="comentario" class="form-control" id="inputComentario" placeholder="Cuenta tu experiencia acerca de tu estancia aquí..." rows="2"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-10 col-md-offset-2">
                                                <!-- Solo se usa para validar las estrellas comenRatingValue-->
                                                <input id="comenRatingValue" name="comenRatingValue" class="hidden"/>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <h2><xsl:value-of select='//text[@id="22"]/*[name()=$language]'/></h2>
                            <div id="googleMap{$currId}" class="map"></div>
                        </div>
                    </xsl:for-each>
                </div>
            </body>
            <!-- TIENE QUE ESTAR AL FINAL PARA QUE LE DÉ TIEMPO A RENDERIZAR -->
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsTgB6Aqq5dnrCdx9uHwcadZu3wxxjP3Q" async="true" defer="defer"></script>
        </html>
    </xsl:template>
</xsl:stylesheet>
