<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xsl" version="1.0">
    <xsl:template match='/'>
        <html lang='$language'>
            <head>
                <title>Q for quality</title>
                <meta charset="utf-8" />
                <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css"/>
                <link rel="stylesheet" href="../css/styles.css"/>
                <link rel="stylesheet" href="../css/font-awesome-4.7.0/css/font-awesome.min.css"/>
                <link rel="stylesheet" href="../jquery-ui/jquery-ui.css"/>
                <link rel="stylesheet" type="text/css" href="../star-rating/src/css/star-rating-svg.css"/>
                <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css"/>
                <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css"/>
                <script src="../jquery-ui/external/jquery/jquery.js"></script>
                <script src="../bootstrap/js/bootstrap.min.js"></script>
                <script src="../jquery-ui/jquery-ui.js"></script>
                <script src="../star-rating/src/jquery.star-rating-svg.js"></script>
                <script src="../js/index.js"></script>
            </head>
            <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"/>
                                <span class="icon-bar"/>
                                <span class="icon-bar"/>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a onclick='changeLanguage("de")'><img src="../images/svg/de.svg" height="30"/></a></li>
                                <li><a onclick='changeLanguage("en")'><img src="../images/svg/en.svg" height="30"/></a></li>
                                <li><a onclick='changeLanguage("es")'><img src="../images/svg/es.svg" height="30"/></a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a onclick='callIndexPHP();'><i class="fa fa-home fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="1"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callHotelsPHP();'><i class="fa fa-h-square fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="2"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callPubsPHP();'><i class="fa fa-cutlery fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="3"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callRestaurantsPHP();'><i class="fa fa-glass fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="4"]/*[name()=$language]'/></a></li>
                                <li><a onclick='callSpasPHP();'><i class="fa fa-shower fa-2x" aria-hidden="true"></i><xsl:value-of select='//text[@id="5"]/*[name()=$language]'/></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div class="jumbotron text-center">
                    <h1>Q for quality</h1>
                    <p><xsl:value-of select='//text[@id="0"]/*[name()=$language]'/></p>
                    <div class="row">
                        <div class="col-md-2 col-md-offset-5">
                            <div class="form-group">
                                <select class="form-control" id="service">
                                    <option><xsl:value-of select='//text[@id="2"]/*[name()=$language]'/></option>
                                    <option><xsl:value-of select='//text[@id="3"]/*[name()=$language]'/></option>
                                    <option><xsl:value-of select='//text[@id="4"]/*[name()=$language]'/></option>
                                    <option><xsl:value-of select='//text[@id="5"]/*[name()=$language]'/></option>
                                </select>
                            </div>
                            <form action="" class="search-form" onsubmit="applySearch(); return false;">
                                <div class="form-group has-feedback">
                                    <label for="search" class="sr-only">Search</label>
                                    <input type="text" class="form-control" name="search" id="search" placeholder="search"/>
                                    <span class="glyphicon glyphicon-search form-control-feedback"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="container-fluid bg-grey">
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="../images/web/logo_calidadTur.png" class="img-rounded" alt="Calidad Turistica" width="250" height="250"/>
                        </div>
                        <div class="col-sm-8">
                            <h2><xsl:value-of select='//text[@id="6"]/*[name()=$language]'/></h2>
                            <p><xsl:value-of select='//text[@id="7"]/*[name()=$language]'/></p>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="../images/web/lg_luxurySpain.png" class="img-rounded" alt="Calidad Turistica" width="250" height="250"/>
                        </div>
                        <div class="col-sm-8">
                            <h2><xsl:value-of select='//text[@id="8"]/*[name()=$language]'/></h2>
                            <p><xsl:value-of select='//text[@id="9"]/*[name()=$language]'/></p>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <a class="twitter-timeline text-left"  href="https://twitter.com/hashtag/qforquality" data-widget-id="863482184505729024">Tweets sobre #qforquality</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

                        <div class=" text-right"> 
                            <a type="application/rss+xml" href="../data/rss.xml"> 
                                <img src="../images/web/rss.png" width="100" height="100"/> 
                            </a> 
                        </div> 
                    </div>
                </div>

                <footer class="container-fluid text-center">
                    <a href="#myPage" title="To Top">
                        <span class="glyphicon glyphicon-chevron-up"></span>
                    </a>
                    <p>Q for quality</p>
                </footer>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
